import { NgModule, Optional, SkipSelf } from "@angular/core";
import { CommentsService } from './services/comments/comments.service'

@NgModule({
    imports: [],
    providers: [CommentsService],
    exports: [],
    declarations: []
})
export class CoreModelue {
    //this module is imported only in appModule and we provide in it services and the components which used for one tile as footer header
    constructor(@Optional() @SkipSelf() core: CoreModelue) {
        if (core) {
            throw new Error("");
        }
    }
}