export interface IComments {  //define type of comments 
    id: number,
    body: string,
    author: string,
    picture: string,
    postId: number
}