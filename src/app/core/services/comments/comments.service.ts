import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { environment } from './../../../../environments/environment';
import { IComments } from './../../models/comments';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommentsService {

  constructor(private httpClient: HttpClient) { }

  get(): Observable<IComments[]> {//return observable list after getting data from server
    return this.httpClient.get<IComments[]>(`${environment.endpointsApi}comments`);
  }
}
