import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModelue } from './core/core.module';
import { ListComponent } from './components/comments/list/list.component'

@NgModule({
  declarations: [
    AppComponent,
    ListComponent //set component 
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule, //set modules
    CoreModelue
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
