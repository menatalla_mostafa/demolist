import { Component, OnInit } from '@angular/core';
import { IComments } from 'src/app/core/models/comments';
import { CommentsService } from './../../../core/services/comments/comments.service'

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  list: IComments[] = [];
  constructor(private commentsService: CommentsService) { }//inject comments service's instance

  ngOnInit(): void {
    this.commentsService.get().subscribe(response => { //fetch data from json-server db by subscribing the observable and set data in list
      this.list = response;
    })
  }

}
