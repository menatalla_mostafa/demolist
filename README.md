# DemoList

Display Grid of comments .

## Get started

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.

### Clone the repo

```bash
git clone git clone https://menatalla_mostafa@bitbucket.org/menatalla_mostafa/demolist.git
```

### Installation

Install the npm packages described in the package.json and verify that it works:
```bash
npm install
npm start
json-server --watch db.json
```




